#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect.hpp"
#include "opencv2/videoio.hpp"
#include <fstream>
#include <iostream>
#include <string>

using namespace std;
using namespace cv;

int distanceBetween(const Point &p1, const Point &p2);
double setOverlaySize(Mat &overlay, int overlayEyeDist, const Point &leftEyeLoc,
                      const Point &rightEyeLoc);
Point computeLocation(const Point &leftEyeOverlay, const Point &leftEyeLoc,
                      double scaleRatio);
void addOverlay(Mat &frame, const Mat &img, const Point &loc);
void detectAndDisplay(Mat frame, const Mat &overlay,
                      const Point &overlayLeftEye, int overlayEyeDist);

CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;

int main(int argc, const char **argv) {
    CommandLineParser parser(
        argc, argv,
        "{help h||}"
        "{face_cascade|/usr/share/circleface/haarcascades/"
        "haarcascade_frontalface_alt.xml|Path "
        "to face cascade.}"
        "{eyes_cascade|/usr/share/circleface/haarcascades/"
        "haarcascade_eye_tree_eyeglasses.xml|Path to eyes cascade.}"
        "{overlay|/usr/share/circleface/overlay.cfg|Path to overlay overlay "
        "configuration file.}");
    parser.about("\nThis program demonstrates using the cv::CascadeClassifier "
                 "class to detect objects (Face + eyes) in a video stream.\n"
                 "You can use Haar or LBP features.\n\n");
    parser.printMessage();

    String face_cascade_name =
        samples::findFile(parser.get<String>("face_cascade"));
    String eyes_cascade_name =
        samples::findFile(parser.get<String>("eyes_cascade"));

    //-- 1. Load the cascades
    if (!face_cascade.load(face_cascade_name)) {
        cout << "--(!)Error loading face cascade\n";
        return -1;
    };

    if (!eyes_cascade.load(eyes_cascade_name)) {
        cout << "--(!)Error loading eyes cascade\n";
        return -1;
    };

    VideoCapture capture;
    //-- 2. Read the video stream
    capture.open(
        "v4l2src ! video/x-raw,format=YUY2,width=480,height=360,framerate=30/1 "
        "! videoconvert ! appsink",
        CAP_GSTREAMER);
    if (!capture.isOpened()) {
        cout << "--(!)Error opening video capture\n";
        return -1;
    }

    string cfgFilename = parser.get<string>("overlay");
    ifstream overlaycfg;
    overlaycfg.open(cfgFilename, ifstream::in);
    if (!overlaycfg.good()) {
        cerr << "--(!)Error couldn't load the overlay configuration file\n";
        return -1;
    }

    String overlayPath;
    pair<Point, Point> overlayEyePos;

    overlaycfg >> overlayPath;
    overlaycfg >> overlayEyePos.first.x >> overlayEyePos.first.y;
    overlaycfg >> overlayEyePos.second.x >> overlayEyePos.second.y;

    overlaycfg.close();

    Mat overlayImg = imread(overlayPath, CV_8UC3);
    if (overlayImg.empty()) {
        cerr << "--(!)Error couldn't load the overlay image\n";
        return -1;
    }

    Mat frame;
    while (capture.read(frame)) {
        if (frame.empty()) {
            cout << "--(!) No captured frame -- Break!\n";
            break;
        }

        //-- 3. Apply the classifier to the frame
        int eyeDist =
            distanceBetween(overlayEyePos.first, overlayEyePos.second);
        detectAndDisplay(frame, overlayImg, overlayEyePos.first, eyeDist);
        if (waitKey(10) == 27) {
            break; // escape
        }
    }

    return 0;
}

/*
 * Compute distance between two points.
 */
int distanceBetween(const Point &p1, const Point &p2) {
    int distance;

    distance =
        sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));

    return distance;
}

/*
 * Compute the scaling ratio to apply on the overlay
 * overlayEyeDist must be different than 0 (doesn't work for cyclopes, sorry)
 */
double setOverlaySize(Mat &overlay, int overlayEyeDist, const Point &leftEyeLoc,
                      const Point &rightEyeLoc) {
    int eyeDist;
    double ratio;

    /* Compute distance between the eyes */
    eyeDist = distanceBetween(leftEyeLoc, rightEyeLoc);

    ratio = (double)eyeDist / (double)overlayEyeDist;

    resize(overlay, overlay, Size(), ratio, ratio);

    return ratio;
}

/*
 * Compute the location where should be placed the top-left corner of the
 * overlay image
 * Both coordinates of the result may be negative. This should be handled in
 * the rendering function
 */
Point computeLocation(const Point &leftEyeOverlay, const Point &leftEyeLoc,
                      double scaleRatio) {
    int locx, locy;
    Point location;

    // Must add the effect of angle
    locx = leftEyeLoc.x - scaleRatio * leftEyeOverlay.x;
    locy = leftEyeLoc.y - scaleRatio * leftEyeOverlay.y;

    location = Point(locx, locy);

    return location;
}

void addOverlay(Mat &frame, const Mat &img, const Point &loc) {
    for (int y = std::max(loc.y, 0); y < frame.rows; ++y) {
        int fY = y - loc.y; // because of the translation

        // we are done of we have processed all rows of the foreground
        // image.
        if (fY >= img.rows)
            break;

        // start at the column indicated by location,
        // or at column 0 if location.x is negative.
        for (int x = std::max(loc.x, 0); x < frame.cols; ++x) {
            int fX = x - loc.x; // because of the translation.

            // we are done with this row if the column is outside of
            // the foreground image.
            if (fX >= img.cols)
                break;

            // determine the opacity of the foregrond pixel, using
            // its fourth (alpha) channel.
            double opacity =
                ((double)img.data[fY * img.step + fX * img.channels() + 3]) /
                255.;

            // and now combine the background and foreground pixel,
            // using the opacity,
            // but only if opacity > 0.
            for (int c = 0; opacity > 0 && c < frame.channels(); ++c) {
                unsigned char foregroundPx =
                    img.data[fY * img.step + fX * img.channels() + c];
                unsigned char backgroundPx =
                    frame.data[y * frame.step + x * frame.channels() + c];
                frame.data[y * frame.step + frame.channels() * x + c] =
                    foregroundPx;
            }
        }
    }
}

void detectAndDisplay(Mat frame, const Mat &overlay,
                      const Point &leftEyeOverlay, int overlayEyeDist) {
    Mat frame_gray;
    cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
    equalizeHist(frame_gray, frame_gray);

    //-- Detect faces
    std::vector<Rect> faces;
    face_cascade.detectMultiScale(frame_gray, faces);
    for (size_t i = 0; i < faces.size(); i++) {
        Point center(faces[i].x + faces[i].width / 2,
                     faces[i].y + faces[i].height / 2);
        Mat faceROI = frame_gray(faces[i]);

        //-- In each face, detect eyes
        std::vector<Rect> eyes;
        eyes_cascade.detectMultiScale(faceROI, eyes);

        // FIXME Eyes not necessary in that order
        Point leftEyeCenter(faces[i].x + eyes[0].x + eyes[0].width / 2,
                            faces[i].y + eyes[0].y + eyes[0].height / 2);
        Point rightEyeCenter(faces[i].x + eyes[1].x + eyes[1].width / 2,
                             faces[i].y + eyes[1].y + eyes[1].height / 2);

        //-- Add the overlay
        Mat resized(overlay);
        double scaleRatio = setOverlaySize(resized, overlayEyeDist,
                                           leftEyeCenter, rightEyeCenter);
        Point overlayTopLeftCorner =
            computeLocation(leftEyeOverlay, leftEyeCenter, scaleRatio);
        addOverlay(frame, resized, overlayTopLeftCorner);
    }

    //-- Show what you got
    imshow("Capture - Face detection", frame);
}
