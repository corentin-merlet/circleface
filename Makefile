# Makefile

APPNAME=circleface

INCLUDEDIR ?= /usr/local/include
BINDIR ?= /usr/bin

CFLAGS=-std=c++11
CPPFLAGS=-I${INCLUDEDIR}/opencv4
LDFLAGS=-lstdc++ \
    -lm \
	-Wl,--start-group \
	-lopencv_core \
	-lopencv_highgui \
	-lopencv_imgcodecs \
	-lopencv_imgproc \
	-lopencv_objdetect \
	-lopencv_videoio \
	-Wl,--end-group

.PHONY: all install clean

all: ${APPNAME}

${APPNAME}: objectDetection.cpp
	${CC} ${CFLAGS} ${CPPFLAGS} -o ${APPNAME} ${^} ${LDFLAGS}

install:
	install -d ${DESTDIR}/${BINDIR}
	install -m 0755 ${APPNAME} ${DESTDIR}/${BINDIR}

clean:
	@rm -f ${APPNAME}
